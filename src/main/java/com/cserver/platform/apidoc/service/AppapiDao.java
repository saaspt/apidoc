package com.cserver.platform.apidoc.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AppapiDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public String getApiDoc(String name){
        String doc=null;

        String sql="SELECT t.`value` FROM api_doc t WHERE t.`name`= ?";

        doc = this.jdbcTemplate.queryForObject(sql, new Object[]{name}, String.class);

        return doc;
    }

}
