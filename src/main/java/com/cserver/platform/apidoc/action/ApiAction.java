package com.cserver.platform.apidoc.action;


import com.alibaba.fastjson.JSONObject;
import com.cserver.platform.apidoc.service.AppapiDao;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApiAction {

    @Autowired
    private AppapiDao appapiDao;

    @GetMapping("/doc/{docname}")
    public  String index(@PathVariable String docname,ModelMap modelMap){

        String doc = appapiDao.getApiDoc(docname);
        if(StringUtils.isEmpty(doc)){
            return "404";
        }
        System.out.println("doc="+doc);

        JSONObject jsonObject = JSONObject.parseObject(doc);
        modelMap.put("apidoc",jsonObject);


        return "index";
    }
}
